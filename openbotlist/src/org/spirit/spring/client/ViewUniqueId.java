/**
 * Berlin Brown
 * Dec 14, 2006
 */
package org.spirit.spring.client;

import org.spirit.util.BotListUniqueId;

/**
 * This is class is used by botverse.
 * @author Berlin Brown
 *
 */
public class ViewUniqueId {

	 public static void main(String[] args) throws Exception {
		 
	        System.out.println("running...");
	        System.out.println("Unique Id(): " 
	        			+ BotListUniqueId.getUniqueId());
	       
	 }
}
